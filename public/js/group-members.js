'use strict'

window.GLANCE = window.GLANCE || {};

(function() {

  const access_level = {
    '10': 'Guest',
    '20': 'Reporter',
    '30': 'Developer',
    '40': 'Maintainer',
    '50': 'Owner',
  }

  let token
  let template
  let users = {}
  const csvlines = []

  async function getAPI( slug, url = 'https://gitlab.com/api/v4/' ) {
    const res = await fetch( url + slug, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': token,
      },
    } )

    if( res.status >= 400 ) {
      if( res.status === 401 ) throw 'Access token not accepted'
      else if( res.status === 404 ) throw slug + ' not found'
      else throw slug + ' ' + res.statusText
    }
    return await res.json()
  }

  async function getMembers( query ) {

    const result = await getAPI( query.query + '/members' )
    for( let i = 0; i < result.length; i++ ) {
      const member = result[i]
      if( member.state !== 'active' ) continue
      const resource = { query: query, access_level: member.access_level }
      if( !users[member.id] ) {
        const user = { id: member.id, username: member.username, name: member.name }
        user.resources = []
        users[member.id] = user
      }
      users[member.id].resources.push( resource )
    }
  }


  async function getGroupMembers( group ) {
    const queries = []
    const groupInfo = await getAPI( `groups/${group}` )
    queries.push( {
      type: 'Group',
      query: `groups/${group}`,
      name: groupInfo.full_name || groupInfo.name,
      description: groupInfo.description,
      visibility: groupInfo.visibility,
    } )
    const projects = groupInfo.projects
    for( let i = 0; i < projects.length; i++ ) {
      const project = projects[i]
      queries.push( {
        type: 'Project',
        query: `projects/${project.id}`,
        name: project.name_with_namespace,
        description: project.description,
        visibility: project.visibility,
      } )
    }
    for( let i = 0; i < queries.length; i++ ) {
      await getMembers( queries[i] )
    }
  }

  function insertText( tpl, text ) {
    const parent = tpl.parentNode
    const clone = document.importNode( tpl.content, true )
    const td = clone.querySelectorAll( 'td' )
    const count = Math.min( text.length, td.length )
    for( let i = 0; i < count; i++ ) {
      td[i].textContent = text[i]
    }
    parent.appendChild( clone )
  }

  async function doReport( groups ) {
    for( let i = 0; i < groups.length; i++ ) {
      try {
        await getGroupMembers( groups[i].trim() )
      }
      catch ( err ) {
        insertText( template, ['', '', 'Error: ' + err] )
      }
    }
    return users
  }

  function order( users ) {
    const names = []
    for( let key in users ) {
      if( users.hasOwnProperty( key ) ) {
        names.push( { name: users[key].name, id: key } )
      }
    }
    const ordered = []
    names.sort( ( a, b ) => a.name.localeCompare( b.name ) )
    for( let i = 0; i < names.length; i++ ) {
      const id = names[i].id
      const user = users[id]
      ordered.push( user )
    }
    return ordered
  }

  function render( users ) {
    for( let i = 0; i < users.length; i++ ) {
      const user = users[i]
      const privates = []
      const publics = []
      for( let j = 0; j < user.resources.length; j++ ) {
        const resource = user.resources[j]
        let str = `${resource.query.name} (${access_level[resource.access_level]})`
        str = str.replace( / +/g, '\u202f' )
        str = str.replace( /-/g, '\u2011' )
        if( resource.query.visibility === 'public' ) publics.push( str )
        else privates.push( str )
        const csvrow = []
        csvrow.push( user.name )
        csvrow.push( user.username )
        csvrow.push( resource.query.visibility === 'public' ? 'No' : 'Yes' )
        csvrow.push( access_level[resource.access_level] )
        const splits = resource.query.name.split( '/' )
        csvrow.push( splits[0] || '?' )
        splits.shift()
        const proj = splits && splits.length > 0 ? splits.join( '/' ) : ''
        csvrow.push( proj )
        csvlines.push( csvrow )
      }
      insertText( template, [user.name, user.username, privates.join( '\u2003' ), publics.join( '\u2003' )] )
    }
  }

  GLANCE.report = function report( groups, token_in, template_in, button, title, date ) {
    groups = document.getElementById( groups ).value
    if( !groups || typeof groups !== 'string' || groups.length <= 0 ) return
    const groupList = groups.split( ',' )

    token = document.getElementById( token_in ).value
    if( !token || typeof token !== 'string' || token.length <= 0 ) return

    template = document.getElementById( template_in )
    button = document.getElementById( button )
    button.disabled = true

    title = document.getElementById( title )
    title.textContent = groupList.join( ', ' )
    date = document.getElementById( date )
    const now = new Date( Date.now() )
    date.textContent = now.toLocaleDateString( 'en-US' )

    csvlines.length = 0
    csvlines.push( ['Name', 'Username', 'Private', 'Role', 'Group', 'Repository'] )

    doReport( groupList )
    .then( order )
    .then( render )
    .then( () => {
      button.disabled = false

      const csvLink = document.getElementById( 'csvButton' )
      const csvContent = csvlines.map( row => row.map( item => '"' + item + '"' )
      .join( "," ) )
      .join( "\n" )
      const csv = new Blob( [csvContent], { type: 'text/csv;charset=utf-8;' } )
      csvLink.href = URL.createObjectURL( csv )
      csvLink.download = 'gitlab-users_' + groups.replace( /[ ,-]/, '_' ) + '.csv'
      csvLink.classList.remove( "collapse" )

      const printLink = document.getElementById( 'printButton' )
      if( printLink ) printLink.classList.remove( "collapse" )
    } )
  }
})()


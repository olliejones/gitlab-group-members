# Gitlab Group Members

Do you need a list of all members of your groups and the projects in them?
This simple web page and Javascript program generates a useful report.

Load [public/index.html](https://olliejones.gitlab.io/gitlab-group-members/) in your favorite browser,
enter a list of group names,
and your Gitlab API Access Token.

Click Generate.

You'll get a report of all users with access to your groups
and the repositories in them.

This uses the Gitlab APIs for
[/groups](https://docs.gitlab.com/ee/api/groups.html),
[/projects](https://docs.gitlab.com/ee/api/README.html#project-resources), and
[/members](https://docs.gitlab.com/ee/api/members.html). 

## Why is this safe?
It only uses GET operations, never any PUT or POST operations.

It's a static HTML page that runs [Javascript](https://olliejones.gitlab.io/gitlab-group-members/js/group-members.js) to retrieve data from Gitlab APIs 
to generate a report. If you want the report saved, you must print it or download the html.
This web page never saves any of your data.

This may be useful for auditing access, or for compliance purposes.
That's why I created it.

(It doesn't work in Internet Explorer.)
